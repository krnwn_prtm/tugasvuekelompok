<?php

namespace App\Http\Controllers;

use App\Models\Suplier;
use Illuminate\Http\Request;

class SuplierController extends Controller
{
    function index()
    {
        $suplier = Suplier::get();
        return response()->json($suplier, 200);
    }

    function getSuplierByID($id)
    {
        $suplier = Suplier::get($id);
        return response()->json($suplier, 200);
    }
}
