<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    function index()
    {
        $barang = Barang::get();
        return response()->json($barang, 200);
    }
}
